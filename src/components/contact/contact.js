import React, { useEffect, useContext } from "react";
import { useFormik } from "formik";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faInstagram } from "@fortawesome/free-brands-svg-icons";

import { AuthContext } from "../../Generic/authContext"
import * as classes from "./contact.module.scss"

const Contact = () => {
    const { storeData, setStoreData } = useContext(AuthContext);

    useEffect(() => {
        setStoreData({ ...storeData, Location: "contact" })
    },[])
    
    const formik = useFormik({
        initialValues: {
            name: "",
            contactemail: "",
            message: "",
        },
        onSubmit: (values) => {
            console.log(values);
        },
    });
    
    return (
        <div className={classes.contact}>
            <h1 className={classes.header}>Contact</h1>
            <div className={classes.page}>
                <form className={classes.form} onSubmit={formik.handleSubmit}>
                    <div className={classes.element}>
                        <label className={classes.label} >NAME</label>
                        <input id="name" name="name" type="text" onChange={formik.handleChange} value={formik.values.name} className={classes.input} />
                    </div>
                    <div className={classes.element}>
                        <label className={classes.label} >EMAIL</label>
                        <input id="contactemail" name="contactemail" type="email" onChange={formik.handleChange} value={formik.values.email} className={classes.input} />
                    </div>
                    <div className={classes.element}>
                        <label className={classes.label} >MESSAGE</label>
                        <textarea id="message" name="message" onChange={formik.handleChange} value={formik.values.message} className={classes.input} style={{ height: "300px" }} />
                    </div>
                    <div style={{ display: "flex", justifyContent: "flex-end", width: "80%"}}>
                        <button type="submit" >Send</button>
                    </div>
                </form>
                <div className={classes.data}>
                    <p>2345 23rd st.N.Birmingham,AL 35203</p>
                    <p>555.365.0987</p>
                    <p>hi@repsy.com</p>
                    <div className={classes.social}>
                        <a href="https://www.facebook.com/abdo.mostafa.319" target="_blank" rel="noopener noreferrer">
                            <FontAwesomeIcon icon={faInstagram} size="2x" color="#64fecc" />
                        </a>
                        <a href="https://www.facebook.com/abdo.mostafa.319" target="_blank" rel="noopener noreferrer">
                            <FontAwesomeIcon icon={faFacebook} size="2x" color="#64fecc" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Contact;
