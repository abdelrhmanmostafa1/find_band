import React from "react"
import { Dialog, DialogTitle, IconButton } from '@material-ui/core';

import * as classes from "./dialoge.module.scss"
import Close from "../../../assets/Cancel.svg"

const Dialoge = props => {
    const { children, open, handleClose } = props
    return (
        <Dialog className={classes.dummy} onClose={handleClose} classes={{paper: classes.dialog}} open={open} id="dialoge" scroll="body" >
            <DialogTitle disableTypography className={classes.title} >
                <IconButton aria-label="Close" size="small" onClick={handleClose} style={{width: "45px"}}>
                    <img src={Close} alt="close" className={classes.image} />
                </IconButton>
            </DialogTitle>
            {children}
        </Dialog>
    )
}

export default Dialoge