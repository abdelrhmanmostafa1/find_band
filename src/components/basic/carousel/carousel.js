import React, { useState, useEffect } from "react"
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord'
import FiberManualRecordOutlinedIcon from '@material-ui/icons/FiberManualRecordOutlined';
import IconButton from '@material-ui/core/IconButton'

import "./carousel.scss"

const Carousel = ({ children }) => {
    const [current, setCurrent] = useState(0)

    useEffect(() => {
        const time = setInterval(() => setCurrent(prevstat => (prevstat + 1 ) % children.length), 5000)
        return () => clearInterval(time)
    }, [current])

    return (
        <>
            {/* class for a slider active card --> active-card-${Data[current].id} */}
            <div className={`carousel`}>
                <div className="cardsWrapper" style={{
                    "transform": `translateX(-${(current * (100/children.length))}%)`,
                    width: `${children.length * 102}%`
                    }}>
                    {React.Children.map(children, child => child)}
                </div>
            </div>
            <div style={{textAlign: "center"}}>
                {new Array(children.length).fill(0).map((item, index) => (
                    <IconButton key={index} style={{width: "65px", padding: 0, color: "#64fecc"}} onClick={() => {
                        setCurrent(index);
                    }}>
                        {current === index ? <FiberManualRecordIcon key={index} color="inherit" /> : <FiberManualRecordOutlinedIcon key={index} color="inherit" />}
                    </IconButton>
                ))}
            </div>
        </>
    )
}

export default Carousel