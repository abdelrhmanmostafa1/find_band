import React, { useState } from "react"

import * as classes from "./resetPassword.module.scss"

const ResetPassword = () => {
    const [email, setEmail] = useState("")

    return(
        <div className={classes.page}>
            <div className={classes.header}>
                <div style={{ fontSize: "250%"}}>FORGOT YOUR PASSWORD?</div>
                <p style={{ marginBottom: "30px" }}>Been there, Done that.</p>
            </div>
            <div className={classes.element}>
                <label className={classes.label} >User Name or Email Address</label>
                <input id="usernameEmail" name="usernameEmail" type="text" onChange={(e) => setEmail(e.target.value)} value={email} className={classes.input} placeholder="Username" />
            </div>
            <button style={{marginBottom: "20px", width: "120px" }} type="submit">Send Link</button>
        </div>
    )
}

export default ResetPassword