import React from "react"
import { useFormik } from "formik";

import * as classes from "./signup.module.scss"

const SignUp = (props) => {
    const { handleClose } = props
    
    const formik = useFormik({
        initialValues: {
            fname: "",
            lname: "",
            signupemail: "",
            username: "",
            password: "",
            passwordre: "",
            role: "booker"
        },
        onSubmit: (values) => {
            console.log(values);
        },
    });

    return (
        <div className={classes.signup}>
            <div className={classes.header}>
                <div style={{ fontSize: "250%"}} >GREAT TO HAVE YOU JOIN THE CREW!</div>
                <p onClick={handleClose} className={classes.subtitle}>Already have an account?</p>
            </div>
            <form onSubmit={formik.handleSubmit} className={classes.form}>
                <div className={classes.row}>
                    <div className={classes.element} style={{ width: "90%", marginRight: "10%" }}>
                        <label className={classes.label} >First Name*</label>
                        <input id="fname" name="fname" type="text" onChange={formik.handleChange} value={formik.values.name} className={classes.input} placeholder="First Name" />
                    </div>
                    <div className={classes.element} style={{ width: "90%" }}>
                        <label className={classes.label} >Last Name*</label>
                        <input id="lname" name="lname" type="text" onChange={formik.handleChange} value={formik.values.name} className={classes.input} placeholder="Last Name" />
                    </div>
                </div>
                <div className={classes.element}>
                    <label className={classes.label} >Email Address*</label>
                    <input id="signupemail" name="signupemail" type="email" onChange={formik.handleChange} value={formik.values.name} className={classes.input} placeholder="Email Address" />
                </div>
                <div className={classes.element}>
                    <label className={classes.label} >Username*</label>
                    <input id="username" name="username" type="text" onChange={formik.handleChange} value={formik.values.name} className={classes.input} placeholder="Username" />
                </div>
                <div className={classes.row}>
                    <div className={classes.element} style={{ width: "90%", marginRight: "10%" }}>
                        <label className={classes.label} >Password*</label>
                        <input id="password" name="password" type="password" onChange={formik.handleChange} value={formik.values.name} className={classes.input} placeholder="Password" />
                    </div>
                    <div className={classes.element} style={{ width: "90%" }}>
                        <label className={classes.label} >Confirm Password*</label>
                        <input id="passwordre" name="passwordre" type="password" onChange={formik.handleChange} value={formik.values.name} className={classes.input} placeholder="Confirm Password" />
                    </div>
                </div>
                <div style={{ width: "100%" }}>
                    <p style={{ marginBottom: "10px" }}>what type of account do you want to create?</p>
                    <div className={classes.choise}>
                        <button
                            className={formik.values.role === "booker" ? classes.active : classes.button}
                            style={{ marginRight: "10%" }}
                            id="rolebooker"
                            name="role"
                            onClick={() => formik.setFieldValue("role", "booker")}>I am booker</button>
                        <button
                            className={formik.values.role === "talent" ? classes.active : classes.button}
                            id="roletalent"
                            name="role"
                            onClick={() => formik.setFieldValue("role", "talent")}>I am talent</button>
                    </div>
                </div>
                <button type="submit">Register</button>
            </form>
            <p>
                By register, i accept the 
                <a href="/" target="_blank" rel="noopener noreferrer">Terms of Service</a>
                 and <a href="/" target="_blank" rel="noopener noreferrer">Privacy Policy</a>
            </p>
        </div>
    )
}

export default SignUp