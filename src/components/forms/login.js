import React from "react"
import { useFormik } from "formik";

import * as classes from "./login.module.scss"

const LogIn = ({ handleClose }) => {
    const formik = useFormik({
        initialValues: {
            usernameEmail: "",
            password: ""
        },
        onSubmit: (values) => {
            console.log(values);
        },
    });

    return (
        <div className={classes.login}>
            <div style={{ fontSize: "250%", width: "100%", marginBottom: "30px", textAlign: "center" }} >LOGIN TO ACCESS YOUR ACCOUNT</div>
            <form onSubmit={formik.handleSubmit} className={classes.form}>
                <div className={classes.element}>
                    <label className={classes.label} >User Name or Email Address</label>
                    <input id="usernameEmail" name="usernameEmail" type="text" onChange={formik.handleChange} value={formik.values.name} className={classes.input} placeholder="Username" />
                </div>
                <div className={classes.element}>
                    <label className={classes.label} >Username*</label>
                    <input id="password" name="password" type="password" onChange={formik.handleChange} value={formik.values.name} className={classes.input} placeholder="Username" />
                </div>
                <button style={{marginBottom: "20px"}} type="submit">Login</button>
                <p onClick={handleClose} className={classes.subtitle}>Forgot Password?</p>
            </form>
        </div>
    )
}

export default LogIn