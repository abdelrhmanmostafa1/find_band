import React, { useEffect, useContext } from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebook, faInstagram } from "@fortawesome/free-brands-svg-icons"
import { AuthContext } from "../../Generic/authContext"

import * as classes from "./about.module.scss"

const About = () => {
    const { storeData, setStoreData } = useContext(AuthContext);

    useEffect(() => {
        setStoreData({ ...storeData, Location: "about" })
    }, [])

    console.log(storeData.Location)
    return(
        <div className={classes.about}>
            <h1 className={classes.header}>About</h1>
            <div className={classes.title}>BRAINS BEHIND THE BRAND</div>
            <p className={classes.subtitle}>We thought: "there got to be a better way to book bands and talents." so we created it.</p>
            <p className={classes.text}>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
            <div className={classes.social}>
                <a href="https://www.facebook.com/abdo.mostafa.319" target="_blank" rel="noopener noreferrer" >
                    <FontAwesomeIcon icon={faInstagram} size="3x" color="#64fecc" />
                </a>
                <a href="https://www.facebook.com/abdo.mostafa.319" target="_blank" rel="noopener noreferrer" >
                    <FontAwesomeIcon icon={faFacebook} size="3x" color="#64fecc" />
                </a>
            </div>
        </div>
    )
}

export default About