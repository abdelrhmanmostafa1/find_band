import React, { useEffect, useContext } from "react"

import { AuthContext } from "../../Generic/authContext"
import Carousel from "../basic/carousel/carousel"
import Card from "./card"
import Quotes from "./quote"
import Tutorial from "./tutorial"
import * as classes from "./home.module.scss"
import Image from "../../assets/ImagePlaceholder.png"

const data = [
    {id: 0, name: "bo5a", genre: "test", bio: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", image: Image, featured_song: "juice"},
    {id: 1, name: "7moksha", genre: "leeeeeeh", bio: "somereally really really really really really really really really hvjsvca kj baskjbcsbc jkbkab khbja shsvajb ahskhsbkabhcbakhbkahsbh hbkjabhbabchabkcjsb jbbcakjbkcabkjab hb kahsbckb kbjcakbjsbchjbkajbcksc really really really really really really long bio", image: Image, featured_song: "juice"},
    {id: 2, name: "band", genre: "slam", bio: "dank dank dank dank dank dank dank dank dank dank dank dank dank dank", image: Image, featured_song: "juice"},
]

const quotes = [
    { id: 0, quote: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.", owner: "Bo5a, hehe"},
    { id: 1, quote: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.", owner: "Bo5a, hehe"},
    { id: 2, quote: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.", owner: "Bo5a, hehe"},
    { id: 3, quote: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.", owner: "Bo5a, hehe"},
    { id: 4, quote: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.", owner: "Bo5a, hehe"},
]

const Home = () => {
    const { storeData, setStoreData } = useContext(AuthContext);

    useEffect(() => {
        setStoreData({ ...storeData, Location: "home"})
    },[])

    return (
        <div className={classes.home}>
            <div className={classes.sec1}>
                <h1>Find an Artist</h1>
                <div className={classes.sub}>FOR YOUR NEXT EVENT</div>
                <div className={classes.search}>
                    <input />
                    <button>Let's Rock!</button>
                </div>
                <p>Are you and artist? join for <a href="/">FREE</a></p>
            </div>
            <div className={classes.sec2}>
                <h1>Featured Artists</h1>
                <div className={classes.carouselWrapper}>
                    <Carousel>
                        {data.map((item, index) => <Card key={index} {...item} />)}
                    </Carousel>
                </div>
                <div className={classes.howItWorks}>
                    <h1>How it Works</h1>
                    <Tutorial />
                </div>
                <div className={classes.quotesWrapper}>
                    <Carousel>
                        {quotes.map((item, index) => <Quotes key={index} {...item} />)}
                    </Carousel>
                </div>
            </div>
        </div>
    )
}

export default Home