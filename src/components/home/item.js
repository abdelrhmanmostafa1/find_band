import React from "react"

import * as classes from "./home.module.scss"

const Step = (props) => {
    const { header, iamge, title, text } = props
    return (
        <div className={classes.step}>
            <div className={classes.header}>{header}</div>
            <img src={iamge} alt={header} />
            <div className={classes.title}>{title}</div>
            <p>{text}</p>
        </div>
    )
}

export default Step