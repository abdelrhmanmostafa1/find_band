import React from "react"

import Item from "./item"
import BOOK from "../../assets/Book.png"
import SEARCH from "../../assets/Search.png"
import PARTY from "../../assets/Party.png"
import * as classes from "./home.module.scss"

const data = [
    { header: "STEP1", iamge: SEARCH, title: "SEARCH", text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat." },
    { header: "STEP2", iamge: BOOK, title: "BOOK", text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat." },
    { header: "STEP2", iamge: PARTY, title: "PARTY", text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat." }
]

const Tutorial = () => {

    return (
        <div className={classes.tutorial}>
            <div className={classes.steps}>
                {data.map((item, index) => <Item key={index} { ...item } />)}
            </div>
            <button className={classes.button}>Sign Up</button>
        </div>
    )
}

export default Tutorial