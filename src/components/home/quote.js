import React from "react"

import * as classes from "./home.module.scss"

const Quote = (props) => {
    const { quote, owner } = props
    return (
        <div className={classes.quotes}>
            <p className={classes.quote}>{quote}</p>
            <p className={classes.owner}>-{owner}</p>
        </div>
    )
}

export default Quote