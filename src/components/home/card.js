import React from "react"

import "./card.scss"
import Icon from "../../assets/Play.svg"
const Card = (props) => {
    const { name, image, id, bio, featured_song, genre } = props
    return (
        <div style={{width: "90%", height: "80%", paddingLeft: "20px"}}>
            <div id={`card-${id}`} className="card">
                <img src={image} alt={name} className="image" />
                <div className="data">
                    <div className="title">
                        <h1>{name}</h1>
                        <div>{genre}</div>
                    </div>
                    <div style={{paddingRight: "20px", height: "60%", fontSize: "20px"}}>
                        {bio}
                    </div>
                    <div className="footer">
                        <div>
                            <p style={{fontWeight: "bold", fontSize: 20}}>Featured Song</p>
                            <a href="/" className="link">
                                <img src={Icon} alt="Play" />
                                <div>{featured_song}</div>
                            </a>
                        </div>
                        <button>View Artist</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Card;