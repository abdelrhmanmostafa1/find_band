import React from "react"
import Divider from '@material-ui/core/Divider';

import * as classes from "./footer.module.scss"

const Footer = () => {
    return (
        <div className={classes.footer}>
            <p>Copyright Repsy 2019</p>
            <Divider orientation="vertical"
            style={{
                height: "20px",
                width: "2px",
                margin: "2px 10px",
                backgroundColor: "#64fecc"
            }}/>
            <p>Copyright Repsy 2019</p>
            <Divider orientation="vertical"
            style={{
                height: "20px",
                width: "2px",
                margin: "2px 10px",
                backgroundColor: "#64fecc"
            }}/>
            <p>Copyright Repsy 2019</p>
            <Divider orientation="vertical"
            style={{
                height: "20px",
                width: "2px",
                margin: "2px 10px",
                backgroundColor: "#64fecc"
            }}/>
            <p>Copyright Repsy 2019</p>
        </div>
    )
}

export default Footer