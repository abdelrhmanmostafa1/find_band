import React, { useState, useContext } from "react"
import { NavLink, useHistory } from 'react-router-dom';

import * as classes from "./header.module.scss"
import { AuthContext } from '../../Generic/authContext';
import LogoGreen from "../../assets/Repsy Logo Green.svg"
import LogoWhite from "../../assets/Repsy Logo White.svg"
import Dialoge from "../basic/dialoge/dialoge"
import SignUp from "../forms/signupform"
import LogIn from "../forms/login"
import ResetPassword from "../forms/resetPassword"

const Header = () => {
    const { storeData } = useContext(AuthContext)
    const history = useHistory()

    const [openSignUp, setOpenSignUp] = useState(false)
    const [openLogIn, setOpenLogIn] = useState(false)
    const [resetPassword, setResetPassword] = useState(false)

    const closeDialoge = (type) => {
        if (type === "signup")
            setOpenSignUp(false)
        else if(type === "login")
            setOpenLogIn(false)
        else if(type === "reset")
        setResetPassword(false)
    }

    const Login = () => {
        setOpenSignUp(false)
        setOpenLogIn(true)
    }

    const reset = () => {
        setOpenLogIn(false)
        setResetPassword(true)
    }

    return (
        <div className={classes.header}>
            <a href="/" ><img src={storeData.Location === "about" ? LogoWhite : LogoGreen} alt="Repsy Logo" className={classes.logo}/></a>
            <div className={classes.controls}>
                <div className={classes.links}>
                    <NavLink to="/about">
                        <button className={classes.login}>
                            About
                        </button>
                    </NavLink>
                    <NavLink to="/contact">
                        <button className={classes.login}>
                            Contact
                        </button>
                    </NavLink>
                </div>
                <div>
                    <button onClick={() => setOpenLogIn(true)} className={classes.login}>LOGIN</button>
                    <button onClick={() => setOpenSignUp(true)}>SIGN UP</button>
                </div>
            </div>
            <Dialoge open={openSignUp} handleClose={() => closeDialoge("signup")}><SignUp handleClose={Login}/></Dialoge>
            <Dialoge open={openLogIn} handleClose={() => closeDialoge("login")}><LogIn handleClose={reset} /></Dialoge>
            <Dialoge open={resetPassword} handleClose={() => closeDialoge("reset")}><ResetPassword handleClose={reset} /></Dialoge>
        </div>
    )
}

export default Header