import React, { useState } from "react";
import { Redirect, Route } from 'react-router-dom';

import { initializeData, AuthContext } from "./Generic/authContext"
import * as classes from "./App.module.scss";
import HomeBackGround from "./assets/HomeBg.jpg"
import AboutBackGround from "./assets/AboutBackground.jpg"

import Header from "./components/header/header"
import Fooeter from "./components/footer/footer"
import Home from "./components/home/home"
import About from "./components/about/about"
import Contact from "./components/contact/contact"

function App() {
    const [storeData, setStoreData] = useState(initializeData);

    const Styles = storeData.Location === "home" ? { backgroundImage: `url(${HomeBackGround})`, backgroundSize: "100% 100%" } :
        storeData.Location === "about" ? { backgroundImage: `url(${AboutBackGround})`, backgroundSize: "100% 100%" } : {}
    return (
        <AuthContext.Provider value={{ storeData, setStoreData }}>
            <div className={classes.App} style={Styles}>
                <header>
                    <Header />
                </header>
                <Route path="/" exact component={Home} />
                <Route path="/about" exact component={About} />
                <Route path="/contact" exact component={Contact} />
                <footer>
                    <Fooeter />
                </footer>
            </div>
        </AuthContext.Provider>
    );
}

export default App;
