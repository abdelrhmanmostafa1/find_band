import { createContext } from 'react';

export const LS_KEY_AUTH = 'Repsy';

export const initializeData = () => {
	const storedAuth = localStorage.getItem(LS_KEY_AUTH);
	if (!storedAuth) {
		const init = {
			Authenticated: false,
			Token: '',
			Location: 'home'
		};
		return init;
	}
	return JSON.parse(storedAuth);
};

export const AuthContext = createContext({
	setStoreData: val => undefined,
	storeData: {
		Authenticated: false,
		Token: '',
		Location: ''
	}
});

export const storeState = (state) => {
	localStorage.setItem(LS_KEY_AUTH, JSON.stringify(state));
};
