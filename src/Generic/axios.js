import axios from "axios"

export const axiosInstance = axios.create({
    baseURL: "",
    method: "POST",
    headers: {
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest'
    },
    timeout: 1000
})